package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MaxPQ<K> implements Iterable<K> 
{
	private K[] pq;                    
	private int n;                       
	private Comparator<K> comparador;  


	public MaxPQ(int initCapacity) 
	{
		pq = (K[]) new Object[initCapacity + 1];
		n = 0;
	}


	public MaxPQ() 
	{
		this(1);
	}


	public MaxPQ(int initCapacity, Comparator<K> comparator) 
	{
		this.comparador = comparator;
		pq = (K[]) new Object[initCapacity + 1];
		n = 0;
	}


	public MaxPQ(Comparator<K> pComp) 
	{
		this(1, pComp);
	}


	public MaxPQ(K[] keys) 
	{
		n = keys.length;
		pq = (K[]) new Object[keys.length + 1];
		for (int i = 0; i < n; i++)
			pq[i+1] = keys[i];
		for (int k = n/2; k >= 1; k--)
			sink(k);
		assert isMaxHeap();
	}



	public boolean isEmpty() 
	{
		return n == 0;
	}

	public int size() 
	{
		return n;
	}


	public K max()
	{
		if (isEmpty())
			throw new NoSuchElementException("Esta vacia.");
		return pq[1];
	}

	private void resize(int capacity) 
	{
		assert capacity > n;
		K[] temp = (K[]) new Object[capacity];
		for (int i = 1; i <= n; i++) 
		{
			temp[i] = pq[i];
		}
		pq = temp;
	}



	public void insert(K x) 
	{

		if (n == pq.length - 1) 
			resize(2 * pq.length);

		pq[++n] = x;
		swim(n);
		assert isMaxHeap();
	}

	public K delMax()
	{
		if (isEmpty())
			throw new NoSuchElementException("Cola vacia.");

		K max = pq[1];
		exch(1, n--);
		sink(1);
		pq[n+1] = null;     
		if ((n > 0) && (n == (pq.length - 1) / 4)) 
			resize(pq.length / 2);
		assert isMaxHeap();
		return max;
	}


	private void swim(int k) 
	{
		while (k > 1 && less(k/2, k)) 
		{
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) 
	{
		while (2*k <= n) 
		{
			int j = 2*k;
			if (j < n && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private boolean less(int i, int j) 
	{
		if (comparador == null) 
		{
			return ((Comparable<K>) pq[i]).compareTo(pq[j]) < 0;
		}
		else {
			return comparador.compare(pq[i], pq[j]) < 0;
		}
	}

	private void exch(int i, int j) 
	{
		K swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}

	private boolean isMaxHeap() 
	{
		return isMaxHeap(1);
	}

	private boolean isMaxHeap(int k) 
	{
		if (k > n)
			return true;

		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= n && less(k, left)) 
			return false;
		if (right <= n && less(k, right)) 
			return false;
		return isMaxHeap(left) && isMaxHeap(right);
	}


	public Iterator<K> iterator() 
	{
		return new HeapIterator();
	}
	private class HeapIterator implements Iterator<K> 
	{

		private MaxPQ<K> copy;


		public HeapIterator()
		{
			if (comparador == null) 
				copy = new MaxPQ<K>(size());
			else                    
				copy = new MaxPQ<K>(size(), comparador);
			for (int i = 1; i <= n; i++)
				copy.insert(pq[i]);
		}

		public boolean hasNext() 
		{ 
			return !copy.isEmpty();                    
		}

		public void remove()      
		{ 
			throw new UnsupportedOperationException();  
		}

		public K next() 
		{
			if (!hasNext()) 
				throw new NoSuchElementException();
			return copy.delMax();
		}
	}
}
