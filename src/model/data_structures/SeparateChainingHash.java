package model.data_structures;

public class SeparateChainingHash<K, V>  implements IHashTable<K, V>
{
    private static final int TAM = 4;

    /**
     * Numero de pares de llave-valor
     */
    private int n;   
    /**
     * Tamaño de la tabla.
     */
    private int m; 
    /**
     * Arreglo de los simbolos
     */
    private Search<K, V>[] st;  

    /**
     * Inicializa una tabla vacia.
     */
    public SeparateChainingHash() 
    {
        this(TAM);
    } 

    
    public SeparateChainingHash(int m) 
    {
        this.m = m;
        st = (Search<K, V>[]) new Search[m];
        for (int i = 0; i < m; i++)
            st[i] = new Search<K, V>();
    } 

   
    private void resize(int chains) 
    {
        SeparateChainingHash<K, V> temp = new SeparateChainingHash<K, V>(chains);
        
        for (int i = 0; i < m; i++) 
        {
            for (K key : st[i].keys()) 
            {
                temp.put(key, st[i].get(key));
            }
        }
        this.m  = temp.m;
        this.n  = temp.n;
        this.st = temp.st;
    }

   
    private int hash(K key) 
    {
        return (key.hashCode() & 0x7fffffff) % m;
    } 

    
    public int size() 
    {
        return n;
    } 

   
    public boolean isEmpty() 
    {
        return size() == 0;
    }

    
    public boolean contains(K key) 
    {
        if (key == null) 
        	throw new IllegalArgumentException("El argumento es nulo.");
        
        return get(key) != null;
    } 

  
    public V get(K key) 
    {
        if (key == null)
        	throw new IllegalArgumentException("El argumento es nulo.");
        int i = hash(key);
        return st[i].get(key);
    } 

    public void put(K key, V val) 
    {
        if (key == null) 
        	throw new IllegalArgumentException("El argumento es nulo.");
        if (val == null) 
        {
            delete(key);
            return;
        }

        if (n >= 10*m) 
        	resize(2*m);

        int i = hash(key);
        if (!st[i].contains(key)) 
        	n++;
        st[i].put(key, val);
    } 

   
    public void delete(K key) 
    {
        if (key == null) 
        	throw new IllegalArgumentException("El argumento es nulo.");

        int i = hash(key);
        if (st[i].contains(key))
        	n--;
        st[i].delete(key);

        if (m > TAM && n <= 2*m) 
        	resize(m/2);
    } 

   
    public Iterable<K> keys()
    {
        Queue<K> queue = new Queue<K>();
        for (int i = 0; i < m; i++) 
        {
            for (K key : st[i].keys())
                queue.enqueue(key);
        }
        return queue;
    } 
}
