package model.data_structures;

public interface IHashTable<K,V>
{
	public void put(K k, V v);

	public V get(K k);

	public void delete (K k);

	public Iterable<K> keys();
	
	public int size();
}
