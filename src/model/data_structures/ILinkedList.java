package model.data_structures;

public interface ILinkedList<T> extends Iterable<T> 
{
	/**
	 * Retorna el tamaño de la lista.
	 * @return
	 */
	public Integer getSize();
	/**
	 * Indica si la lista esta vacia o no.
	 * @return
	 */
	public boolean isEmpty();
	/**
	 * Retorna el primer elemento del arreglo.
	 * @return
	 */
	public T darPrimero();
	/**
	 * Retorna el ultimo elemento del arreglo.
	 */
	public T darUltimo();
	/**
	 * Agrega un elemento entre 2 elementos.
	 */
	public void agregarEntre(T pElm, NodeLinkedList<T> pP, NodeLinkedList<T> pS);
	/**
	 * Agrega un elemento
	 */
	public void agregarAlInicio(T dato);

	/**
	 * Agregar un elemento al final
	 * @param dato
	 */
	public void agregarAlFinal(T dato);
	/**
	 * Elimina un elemento.
	 * @param dato
	 */
	public T eliminar(NodeLinkedList<T> nodo);
}
