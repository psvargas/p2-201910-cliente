package model.data_structures;
import java.io.Serializable;
import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T>, Serializable
{

	private NodeLinkedList<T> cabeza;
	private NodeLinkedList<T> cola;
	private int tamano = 0;

	public LinkedList()
	{
		cabeza = new NodeLinkedList<T>(null, null, cola);
		cola = new NodeLinkedList<T>(null, cabeza, null);
	}
	public Integer getSize() 
	{
		return tamano;
	}
	public boolean isEmpty()
	{
		return tamano == 0;
	}

	public T darPrimero()
	{
		if(isEmpty() == true)
			return null;
		else
			return cabeza.darSiguiente().darElemento();
	}

	public T darUltimo()
	{
		if(isEmpty() == true)
			return null;
		else
			return cola.darAnterior().darElemento();
	}

	public void agregarEntre(T elemento, NodeLinkedList<T> primero, NodeLinkedList<T> segundo)
	{
		NodeLinkedList<T> nuevo = new NodeLinkedList<T>(elemento, primero, segundo);
		primero.cambiarSiguiente(nuevo);
		segundo.cambiarAnterior(nuevo);
		tamano++;
	}
	public void agregarAlInicio(T pElemento) 
	{
		agregarEntre(pElemento, cabeza, cabeza.darSiguiente());
	}

	public void agregarAlFinal(T pElemento) 
	{
		agregarEntre(pElemento, cola.darAnterior(), cola);
	}

	public T eliminar(NodeLinkedList<T> nodo)
	{
		if(nodo != null)
		{
			NodeLinkedList<T> anterior = nodo.darAnterior();
			NodeLinkedList<T> siguiente = nodo.darSiguiente();

			anterior.cambiarSiguiente(siguiente);
			siguiente.cambiarAnterior(anterior);
			nodo.cambiarSiguiente(null);
			nodo.cambiarAnterior(null);
			tamano--;
		}
			return nodo.darElemento();
		
	}

	public Iterator<T> iterator() 
	{
		return new IteradorLinkedList<>(cabeza.darSiguiente());
	}
}
