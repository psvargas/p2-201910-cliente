package model.data_structures;

import java.util.NoSuchElementException;


public class BST<K extends Comparable<K>, V>
{

	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	private Nodo raiz;    


	private class Nodo 
	{
		private K key;           
		private V val;      
		private Nodo left, right;  
		private boolean color;     
		private int size;          

		public Nodo(K key, V val, boolean color, int size)
		{
			this.key = key;
			this.val = val;
			this.color = color;
			this.size = size;
		}
	}


	public BST() 
	{
	}

	private boolean isRed(Nodo x) 
	{
		if (x == null) 
			return false;

		return x.color == RED;
	}


	private int size(Nodo x) 
	{
		if (x == null) 
			return 0;
		return x.size;
	} 


	public int size()
	{
		return size(raiz);
	}


	public boolean isEmpty() 
	{
		return raiz == null;
	}


	public V get(K key) 
	{
		if (key == null)
			throw new IllegalArgumentException("El argumento de nulo.");

		return get(raiz, key);
	}


	private V get(Nodo x, K key)
	{
		while (x != null) 
		{
			int cmp = key.compareTo(x.key);
			if      (cmp < 0)
				x = x.left;
			else if (cmp > 0) 
				x = x.right;
			else             
				return x.val;
		}
		return null;
	}


	public boolean contains(K key) 
	{
		return get(key) != null;
	}


	public void put(K key, V val) 
	{
		if (key == null) 
			throw new IllegalArgumentException("El primer argumento es nulo.");
		if (val == null)
		{
			delete(key);
			return;
		}

		raiz = put(raiz, key, val);
		raiz.color = BLACK;

	}

	private Nodo put(Nodo h, K key, V val) 
	{ 
		if (h == null)
			return new Nodo(key, val, RED, 1);

		int cmp = key.compareTo(h.key);

		if      (cmp < 0)
			h.left  = put(h.left,  key, val); 
		else if (cmp > 0)
			h.right = put(h.right, key, val); 
		else              
			h.val   = val;

		if (isRed(h.right) && !isRed(h.left))     
			h = rotarIzquierda(h);
		if (isRed(h.left)  &&  isRed(h.left.left)) 
			h = rotarDerecha(h);
		if (isRed(h.left)  &&  isRed(h.right))     
			flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;

		return h;
	}


	public void deleteMin() 
	{
		if (isEmpty()) 
			throw new NoSuchElementException("BST underflow");


		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = deleteMin(raiz);

		if (!isEmpty()) 
			raiz.color = BLACK;

	}

	private Nodo deleteMin(Nodo h) 
	{ 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}


	public void deleteMax() 
	{
		if (isEmpty()) 
			throw new NoSuchElementException("BST underflow");


		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = deleteMax(raiz);

		if (!isEmpty()) 
			raiz.color = BLACK;

	}


	private Nodo deleteMax(Nodo h) 
	{ 
		if (isRed(h.left))
			h = rotarDerecha(h);

		if (h.right == null)
			return null;

		if (!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}


	public void delete(K key) 
	{ 
		if (key == null) 
			throw new IllegalArgumentException("argument to delete() is null");

		if (!contains(key)) 
			return;

		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = delete(raiz, key);
		if (!isEmpty()) 
			raiz.color = BLACK;

	}


	private Nodo delete(Nodo h, K key) 
	{ 

		if (key.compareTo(h.key) < 0) 
		{
			if (!isRed(h.left) && !isRed(h.left.left))
				h = moveRedLeft(h);

			h.left = delete(h.left, key);
		}
		else 
		{
			if (isRed(h.left))
				h = rotarDerecha(h);

			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;

			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);

			if (key.compareTo(h.key) == 0) 
			{
				Nodo x = min(h.right);
				h.key = x.key;
				h.val = x.val;
				h.right = deleteMin(h.right);
			}
			else 
				h.right = delete(h.right, key);
		}
		return balance(h);
	}


	private Nodo rotarDerecha(Nodo h) 
	{

		Nodo x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = x.right.color;
		x.right.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}


	private Nodo rotarIzquierda(Nodo h)
	{
		Nodo x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = x.left.color;
		x.left.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}


	private void flipColors(Nodo h) 
	{	
		h.color = !h.color;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}


	private Nodo moveRedLeft(Nodo h) 
	{
		flipColors(h);

		if (isRed(h.right.left)) 
		{ 
			h.right = rotarDerecha(h.right);
			h = rotarIzquierda(h);
			flipColors(h);
		}
		return h;
	}


	private Nodo moveRedRight(Nodo h)
	{
		flipColors(h);

		if (isRed(h.left.left))
		{ 
			h = rotarDerecha(h);
			flipColors(h);
		}
		return h;
	}


	private Nodo balance(Nodo h) 
	{
		if (isRed(h.right))  
			h = rotarIzquierda(h);
		if (isRed(h.left) && isRed(h.left.left))
			h = rotarDerecha(h);
		if (isRed(h.left) && isRed(h.right)) 
			flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}



	public int altura() 
	{
		return altura(raiz);
	}
	private int altura(Nodo x) 
	{
		if (x == null)
			return -1;

		return 1 + Math.max(altura(x.left), altura(x.right));
	}


	public K min()
	{
		if (isEmpty()) 
			throw new NoSuchElementException("Se llama el min sin simbolo en la tabla.");

		return min(raiz).key;
	} 


	private Nodo min(Nodo x) 
	{ 

		if (x.left == null) 
			return x; 
		else                
			return min(x.left); 
	} 


	public K max() 
	{
		if (isEmpty()) 
			throw new NoSuchElementException("Se llama al max sin simbolo en la tabla.");

		return max(raiz).key;
	} 


	private Nodo max(Nodo x) 
	{ 
		if (x.right == null) 
			return x; 
		else                
			return max(x.right); 
	} 


	public K floor(K key) 
	{
		if (key == null)
			throw new IllegalArgumentException("Argumento nulo.");

		if (isEmpty()) 
			throw new NoSuchElementException("Simbolo vacio en la tabla.");

		Nodo x = floor(raiz, key);

		if (x == null) 
			return null;
		else           
			return x.key;
	}    

	private Nodo floor(Nodo x, K key) 
	{
		if (x == null)
			return null;

		int cmp = key.compareTo(x.key);

		if (cmp == 0) 
			return x;
		if (cmp < 0)  
			return floor(x.left, key);

		Nodo t = floor(x.right, key);

		if (t != null) 
			return t; 
		else           
			return x;
	}

	public K ceiling(K key)
	{
		if (key == null) 
			throw new IllegalArgumentException("Argumento nulo.");

		if (isEmpty())
			throw new NoSuchElementException("Simbolo vacio en la tabla.");

		Nodo x = ceiling(raiz, key);

		if (x == null) 
			return null;
		else          
			return x.key;  
	}


	private Nodo ceiling(Nodo x, K key) 
	{  
		if (x == null) 
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) 
			return x;
		if (cmp > 0)  
			return ceiling(x.right, key);
		Nodo t = ceiling(x.left, key);
		if (t != null)
			return t; 
		else           
			return x;
	}


	public K select(int k) 
	{
		if (k < 0 || k >= size())
		{
			throw new IllegalArgumentException("Argumento a seleccionar es inválido: " + k);
		}
		Nodo x = select(raiz, k);
		return x.key;
	}

	private Nodo select(Nodo x, int k) 
	{
		int t = size(x.left); 
		if      (t > k) 
			return select(x.left,  k); 
		else if (t < k) 
			return select(x.right, k-t-1); 
		else           
			return x; 
	} 

	public int rank(K key) 
	{
		if (key == null) 
			throw new IllegalArgumentException("Argumento nulo.");
		return rank(key, raiz);
	} 


	private int rank(K key, Nodo x) 
	{
		if (x == null)
			return 0; 
		int cmp = key.compareTo(x.key); 
		if      (cmp < 0) 
			return rank(key, x.left); 
		else if (cmp > 0) 
			return 1 + size(x.left) + rank(key, x.right); 
		else             
			return size(x.left); 
	} 


	public Iterable<K> keys() 
	{
		if (isEmpty()) 
			return new Queue<K>();
		return keys(min(), max());
	}


	public Iterable<K> keys(K lo, K hi) 
	{
		if (lo == null) 
			throw new IllegalArgumentException("Primer argumento es nulo.");
		if (hi == null) 
			throw new IllegalArgumentException("Segundo argumento es nulo.");

		Queue<K> queue = new Queue<K>();
		keys(raiz, queue, lo, hi);
		return queue;
	} 


	private void keys(Nodo x, Queue<K> queue, K lo, K hi)
	{ 
		if (x == null) 
			return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) 
			keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) 
			queue.enqueue(x.key); 
		if (cmphi > 0) 
			keys(x.right, queue, lo, hi); 
	} 
	
	public Iterable<V> valuesInRange(K lo, K hi) 
    {
        if (lo == null) 
        	throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) 
        	throw new IllegalArgumentException("second argument to keys() is null");

        Queue<V> queue = new Queue<V>();
        keysToValues(raiz, queue, lo, hi);
        return queue;
    } 
	
	private void keysToValues(Nodo x, Queue<V> queue, K lo, K hi) 
    { 
        if (x == null) 
        	return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) 
        	keysToValues(x.left, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) 
        	queue.enqueue(x.val); 
        if (cmphi > 0) 
        	keysToValues(x.right, queue, lo, hi); 
    } 
	

	public int size(K lo, K hi) 
	{
		if (lo == null) 
			throw new IllegalArgumentException("Primer argumento nulo.");
		if (hi == null)
			throw new IllegalArgumentException("segundo argumento nulo.");

		if (lo.compareTo(hi) > 0) 
			return 0;
		if (contains(hi)) 
			return rank(hi) - rank(lo) + 1;
		else             
			return rank(hi) - rank(lo);
	}
	
	private boolean isBalanced() 
	{ 
		int black = 0;    
		Nodo x = raiz;
		while (x != null) 
		{
			if (!isRed(x)) black++;
			x = x.left;
		}
		return isBalanced(raiz, black);
	}


	private boolean isBalanced(Nodo x, int black) 
	{
		if (x == null) 
			return black == 0;
		if (!isRed(x)) black--;
		return isBalanced(x.left, black) && isBalanced(x.right, black);
	} 
}