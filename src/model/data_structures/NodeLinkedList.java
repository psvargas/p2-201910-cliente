package model.data_structures;
import java.io.Serializable;

public class NodeLinkedList<E> implements Serializable
{
	private NodeLinkedList<E> anterior;
	private NodeLinkedList<E> siguiente;

	private E elemento;

	public NodeLinkedList(E pElemento,NodeLinkedList<E> pAnterior, NodeLinkedList<E> pSiguiente)
	{
		elemento = pElemento;
		siguiente = pSiguiente;
		anterior = pAnterior;
	}

	public E darElemento()
	{
		return elemento;
	}

	public NodeLinkedList<E> darAnterior()
	{
		return anterior;
	}

	public NodeLinkedList<E> darSiguiente()
	{
		return siguiente;
	}

	public void cambiarAnterior(NodeLinkedList<E> pElemento)
	{
		anterior = pElemento;
	}

	public void cambiarSiguiente(NodeLinkedList<E> pElemento)
	{
		siguiente = pElemento;
	}
	public void cambiarActual(E pElemento)
	{
		elemento = pElemento;
	}
}

