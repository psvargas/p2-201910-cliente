package model.vo;

import java.util.Comparator;

public class ComparadorViolationCode implements Comparator<InfraccionesViolationCode>
{

	@Override
	public int compare(InfraccionesViolationCode o1, InfraccionesViolationCode o2) 
	{
		if(o1.totalInfracciones < o2.totalInfracciones)
			return -1;
		else if(o1.totalInfracciones > o2.totalInfracciones)
			return 1;
		else
			return 0;
	}
}