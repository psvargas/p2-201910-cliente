package model.vo;

import java.util.Comparator;

public class ComparadorFranjaHoraria implements Comparator<InfraccionesFranjaHoraria>
{
	public int compare(InfraccionesFranjaHoraria o1, InfraccionesFranjaHoraria o2) 
	{
		if(o1.totalInfracciones > o2.totalInfracciones)
		{
			return 1;
		}
		else if(o1.totalInfracciones < o2.totalInfracciones)
		{
			return -1;
		}
		else
			return 0;
	}
	
}
