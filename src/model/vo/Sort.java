package model.vo;
import java.util.*;

public class Sort 
{

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos )
	{
		// TODO implementar el algoritmo ShellSort

		int n = datos.length;
		int h = 1;
		while (h < n/3) 
			h = 3*h + 1; 

		while (h >= 1) 
		{
			for (int i = h; i < n; i++) 
			{
				for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) 
				{
					exchange(datos, j, j-h);
				}
			}
			
			h = h / 3;
		}
	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) 
	{

		Comparable[] aux = new Comparable[datos.length];  
		mergeSort(datos, aux, 0, datos.length-1);
	}
	public static void mergeSort(Comparable[] datos, Comparable[] aux, int low, int high)
	{  
		if (high<=low + 15) 
		{
			for (int i=low; i <= high; i++) 
			{
	            for(int j = i; j > low && less(datos[j], datos[j-1]); j--) 
	            {
	                exchange(datos, j, j-1);
	            }
	        } 
			return;
		}

		int mid = low + (high - low)/2;  
		mergeSort(datos, aux, low, mid);  
		mergeSort(datos, aux, mid+1, high); 

		if(less(datos[mid+1],datos[mid])) 
		{  
			mergeArrays(datos, aux, low, mid, high); 
		}

	}
	
	public static void mergeArrays(Comparable[] datos, Comparable[] aux, int low, int mid, int high)
	{
		int i = low;
		int j = mid+1;

		for (int k = low; k <= high; k++) 
		{  
			aux[k] = datos[k];
		}

		for (int k = low; k <= high; k++)
		{  
			if (i > mid) 
			{  
				datos[k] = aux[j++];
			} 
			else if (j > high)
			{ 
				datos[k] = aux[i++];
			}
			else if (less(aux[j], aux[i]))
			{ 
				datos[k] = aux[j++];
			}
			else
			{  
				datos[k] = aux[i++];
			}
		}
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */

	public static void ordenarQuickSort(Comparable[] datos) 
	{
        quickSort(datos, 0, datos.length - 1);
    }

    public static void quickSort(Comparable[] datos, int low, int high) 
    {       
       
        if (high<=low + 15) 
        {
        	for (int i=low; i <= high; i++) 
			{
	            for(int j = i; j > low && less(datos[j], datos[j-1]); j--) 
	            {
	                exchange(datos, j, j-1);
	            }
	        } 
           return;
        }

        int j = quickPartition(datos, low, high);  
        quickSort(datos, low, j-1); 
        quickSort(datos, j+1, high); 
    }

    private static int quickPartition(Comparable[] datos, int low, int high) 
    {
        int i = low;
        int j = high+1;
        Comparable v = datos[low];

        while(true)
        {
            while(less(datos[++i],v))
            {  
                if(i==high)
                {  
                    break;
                }
            }

            while(less(v, datos[--j])) 
            { 
                if(j==low) 
                {
                    break;
                }
            }
            if(i>=j) 
            { 
                break;
            }
            exchange(datos, i, j);  
        }
        exchange(datos, low, j);  
        return j;  
    }
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		return v.compareTo(w) < 0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Comparable d = datos[i];
		datos[i] = datos[j];
		datos[j] = d;
	}
}