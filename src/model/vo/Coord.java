package model.vo;

public class Coord implements Comparable<Coord>
{

	private String coordX;
	private String coordY;

	public Coord(String x, String y)
	{
		coordX = x;
		coordY = y;
	}


	public String darX()
	{
		return coordX;
	}

	public String darY()
	{
		return coordY;
	}


	@Override
	public int compareTo(Coord o) 
	{
		if(coordX.compareTo(o.darX()) > 0)
			return 1;
		
		else if(coordX.compareTo(o.darX()) < 0)
			return -1;
		
		else
		{
			if(coordY.compareTo(o.darY()) > 0)
				return 1;
			
			else if(coordY.compareTo(o.darY()) < 0)
				return -1;

			return 0;
		}
	}
}