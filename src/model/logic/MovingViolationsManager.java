package model.logic;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.BST;
import model.data_structures.IQueue;
import model.data_structures.LinearProbing;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.vo.ComparadorFranjaHoraria;
import model.vo.ComparadorViolationCode;
import model.vo.Coord;
import model.vo.EstadisticaInfracciones;
import model.vo.EstadisticasCargaInfracciones;
import model.vo.InfraccionesFecha;
import model.vo.InfraccionesFechaHora;
import model.vo.InfraccionesFranjaHoraria;
import model.vo.InfraccionesFranjaHorariaViolationCode;
import model.vo.InfraccionesLocalizacion;
import model.vo.InfraccionesViolationCode;
import model.vo.Sort;
import model.vo.VOMovingViolations;

public class MovingViolationsManager 
{	
	private IQueue<VOMovingViolations> mvQueue;

	private ManejoFechaHora manejoFechaHora;

	private BST infraccHoraArbol;

	private LinearProbing<String, VOMovingViolations> mvHash;
	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		mvQueue = new Queue<VOMovingViolations>();
		manejoFechaHora= new ManejoFechaHora();
		infraccHoraArbol= new BST();
		mvHash= new LinearProbing<>(200);
	}

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public void loadMovingViolations(String ruta1, String ruta2, String ruta3, String ruta4, String ruta5, String ruta6)
	{	
		if(mvQueue.isEmpty())
		{
			CSVReader lector = null;
			String [] rutas = new String[6];
			rutas[0] = ruta1;
			rutas[1] = ruta2;
			rutas[2] = ruta3;
			rutas[3] = ruta4;
			rutas[4] = ruta5;
			rutas[5] = ruta6;

			String xMax = "";
			String yMax = "";
			String xMin =  "ZZZZZZ";
			String yMin = "ZZZZZZZ";

			for(int i = 0; i < rutas.length; i++)
			{
				try 
				{
					lector = new CSVReader(new FileReader(rutas[i]));
					String[] partes = lector.readNext();
					partes = lector.readNext();
					int contador = 0;

					while(partes != null)
					{
						int id = Integer.parseInt(partes[0]);
						String loc = partes[2];
						int add = Integer.parseInt(partes[3]);
						String str = partes[4];
						String x = partes[5];
						String y = partes[6];
						double fin = Double.parseDouble(partes[8]);
						double total = Double.parseDouble(partes[9]);
						double pen = Double.parseDouble(partes[10]);
						String acId = partes[12];
						String date;
						String vc;
						String desc;
						if(partes[13].contains("2018"))
						{
							date=partes[13];
							vc = partes[14];
							desc = partes[15];
						}
						else
						{
							date= partes[14];
							vc = partes[15];
							desc = partes[16];
						}
						VOMovingViolations nuevo = new VOMovingViolations(id, loc, add, str,x,y, fin, total, pen, acId, date, vc, desc);
						mvQueue.enqueue(nuevo);

						partes = lector.readNext();
						contador++;

						if(x.compareTo(xMax) > 0)
							xMax = x;

						if(x.compareTo(xMin) < 0)
							xMin = x;

						if(y.compareTo(yMax) > 0)
							yMax = y;
						if(y.compareTo(yMin) < 0)
							yMin = y;
					}
					lector.close();
					String[] m = rutas[i].split("/");
					String[] me = m[2].split("_");
					String mes = me[0];

					System.out.println("Infracciones mes "+mes+":"+contador);
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			System.out.println("Total de infracciones :"+ mvQueue.size());
			System.out.println("La zona geográfica que contiene las infracciones es: (" + xMin + "," + yMin+") (" + xMax + "," + yMax + ")");
		}
		else
			System.out.println("No se pueden cargar los datos por que ya hay datos cargados.");
	}
	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	 * que tengan m�s infracciones. 
	 * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		IQueue<InfraccionesFranjaHoraria> infracciones= new Queue();

		Queue<VOMovingViolations> copia= (Queue<VOMovingViolations>) mvQueue;
		MaxPQ<InfraccionesFranjaHoraria> pq= new MaxPQ<InfraccionesFranjaHoraria>(new ComparadorFranjaHoraria());

		Iterator<VOMovingViolations> iter= copia.iterator();
		int j=0;
		while(iter.hasNext() && j<=23)
		{
			IQueue<VOMovingViolations> infraHora= new Queue();
			String comp;
			for(int i=0;i<copia.size()-1;i++)
			{
				VOMovingViolations actual= iter.next();
				String[] fecha= actual.getTicketIssueDate().split("T");
				String[] hora= fecha[1].split(":");
				comp= Integer.parseInt(hora[0])<10? "0"+j: ""+j;
				if(hora[0].equals(comp))
				{
					infraHora.enqueue(actual);
				}
			}
			String hour= j<10?"0"+j:""+j;
			InfraccionesFranjaHoraria infraccHora= new InfraccionesFranjaHoraria(manejoFechaHora.convertirHora_LT(hour+":00:00"), manejoFechaHora.convertirHora_LT(hour+":59:59"),infraHora);
			pq.insert(infraccHora);
			iter= copia.iterator();
			j++;
		}
		for(int i=0;i<=N;i++)
		{
			infracciones.enqueue(pq.delMax());
		}
		return infracciones;
	}

	/**
	 * Requerimiento 2A: Consultar  las  infracciones  por
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{
		// TODO completar
		Iterator<VOMovingViolations> it= mvQueue.iterator();
		while(it.hasNext())
		{
			VOMovingViolations actual= it.next();
			mvHash.put(actual.darXCoord()+actual.darYCoord(), actual);
		}
		String locat="";
		int adress=0;
		int street=0;
		InfraccionesLocalizacion retorno;
		IQueue<VOMovingViolations> lista= new Queue();
		Iterable<VOMovingViolations> buscados=  mvHash.get(""+xCoord+yCoord);
		Iterator<VOMovingViolations> iter=buscados.iterator();
		VOMovingViolations primero= iter.next();
		if(primero!=null)
		{
			locat= primero.getLocation();
			adress= primero.getAddressId();
			street= Integer.parseInt(primero.getStreetSegId());
		}
		Iterator<VOMovingViolations> i=buscados.iterator();
		while(i.hasNext())
		{
			VOMovingViolations actual= i.next();
			lista.enqueue(actual);
		}
		retorno= new InfraccionesLocalizacion(xCoord, yCoord, locat, adress, street, lista );
		return retorno;
	}
	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	 * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		// TODO completar
		IQueue<InfraccionesFecha> retorno= new Queue<InfraccionesFecha>();
		Iterator<VOMovingViolations> iter= mvQueue.iterator();
		VOMovingViolations actual= iter.next();
		while(iter.hasNext())
		{
			LocalDate fechaAct=manejoFechaHora.convertirFecha_LD(actual.getTicketIssueDate().split("T")[0]);
			IQueue<VOMovingViolations> infraccFecha= new Queue();
			VOMovingViolations siguiente= iter.next();
			if(manejoFechaHora.convertirFecha_LD(siguiente.getTicketIssueDate().split("T")[0]).compareTo(fechaAct)==0)
			{
				infraccFecha.enqueue(siguiente);
			}
			else
			{
				infraccFecha.enqueue(actual);
				actual=siguiente;
				infraccHoraArbol.put(fechaAct, infraccFecha);
			}
		}
		Iterable <Queue> vals= infraccHoraArbol.valuesInRange(fechaInicial, fechaFinal);
		Iterator<Queue> i= vals.iterator();
		while(i.hasNext())
		{
			Queue actual1= i.next();
			InfraccionesFecha nueva= new InfraccionesFecha(actual1, fechaInicial);
			retorno.enqueue(nueva);
		}
		return retorno;		
	}

	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		MaxPQ<InfraccionesViolationCode> pq = new MaxPQ<InfraccionesViolationCode>(new ComparadorViolationCode());
		IQueue<InfraccionesViolationCode> iRetorno = new Queue<InfraccionesViolationCode>();

		Queue<VOMovingViolations> copia = (Queue<VOMovingViolations>) mvQueue;
		Iterator<VOMovingViolations> iter = copia.iterator();

		while(iter.hasNext())
		{
			Queue<VOMovingViolations> infracciones = new Queue<VOMovingViolations>();

			VOMovingViolations actual = iter.next();
			String code = actual.getViolationCode();

			Iterator<VOMovingViolations> iter2 = iter;
			iter2.next();
			while(iter2.hasNext() && iter2.next().getViolationCode().equals(code) )
			{
				VOMovingViolations a = iter2.next();
				if(a.getViolationCode().equals(code))
					infracciones.enqueue(a);

				//infracciones.enqueue(iter2.next());
				//iter2.next();
			}
			infracciones.enqueue(actual);
			pq.insert(new InfraccionesViolationCode(code, infracciones));
			//iter.next();
		}

		for(int i=0; i<N+1; i++)
		{
			iRetorno.enqueue(pq.delMax());
		}
		return iRetorno;

	}

	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(String xCoord, String yCoord)
	{	
		Coord buscada = new Coord(xCoord, yCoord);

		BST<Coord, InfraccionesLocalizacion> arbol = new BST<>();
		IQueue<VOMovingViolations> copia = mvQueue;
		Iterator<VOMovingViolations> iter = copia.iterator();
		IQueue<VOMovingViolations> h = new Queue<>();
		VOMovingViolations vo = null;

		while(iter.hasNext())
		{
			VOMovingViolations actual = iter.next();
			if(vo==null)
				vo = actual;

			if(vo.darXCoord().compareTo(xCoord) == 0 && vo.darYCoord().compareTo(yCoord)==0)
				h.enqueue(actual);

			else
			{
				Coord coordenada = new Coord(vo.darXCoord(),vo.darYCoord());
				Double x= Double.parseDouble(vo.darXCoord());
				Double y= Double.parseDouble(vo.darYCoord());
				InfraccionesLocalizacion infr = new InfraccionesLocalizacion(x, y, vo.getLocation(), vo.getAddressId(),Integer.parseInt(vo.getStreetSegId()), h);
				arbol.put(coordenada, infr);

				vo = actual;
				h = new Queue<VOMovingViolations>();
				h.enqueue(vo);
			}
		}

		return arbol.get(buscada);

	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{

		BST<Double, InfraccionesFechaHora> arbol = new BST<Double, InfraccionesFechaHora>();
		Iterator<VOMovingViolations> iter = mvQueue.iterator();
		ManejoFechaHora c1 = new ManejoFechaHora();
		VOMovingViolations vo = null;



		while(iter.hasNext())
		{
			IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
			double total = 0;

			VOMovingViolations actual = iter.next();

			if(vo==null)
				vo=actual;


			if(Integer.parseInt(actual.darHora())==Integer.parseInt(vo.darHora()))
			{
				cola.enqueue(actual);
				total+=actual.getTotalPaid();
			}
			else
			{
				String[] j = vo.getTicketIssueDate().split("T");
				String[] h= j[1].split(":");

				LocalDateTime h1 = c1.convertirFecha_Hora_LDT(j[0]+"T"+h[0]+":00:00.000Z");
				LocalDateTime h2 = c1.convertirFecha_Hora_LDT(j[0]+"T"+h[0]+":59:59.000Z");
				InfraccionesFechaHora infrac= new InfraccionesFechaHora(h1, h2, cola);
				arbol.put(total, infrac);

				vo = actual;
			}
		}

		return (IQueue<InfraccionesFechaHora>) arbol.valuesInRange(valorInicial, valorFinal);


	}

	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		LinearProbing<Integer,InfraccionesLocalizacion> tabla = new LinearProbing<Integer,InfraccionesLocalizacion>();
		Iterator<VOMovingViolations> iter = mvQueue.iterator();

		while(iter.hasNext()) 
		{
			VOMovingViolations actual = iter.next();
			int add = actual.getAddressId();
			String stre = actual.getStreetSegId(); 
			Queue<VOMovingViolations> cola = new Queue<VOMovingViolations>();

			Iterator<VOMovingViolations> iter2 = iter;
			while(iter.hasNext() && iter2.next().getAddressId() == add) 
			{
				cola.enqueue(iter2.next());
			}

			InfraccionesLocalizacion inf = new InfraccionesLocalizacion(null, null,null, add, Integer.parseInt(stre), cola);

			try 
			{
				tabla.put(add, inf);
				if(add == addressID)
					return tabla.get(add);

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			iter.next();

		}
		return null;
	}		

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{
		// TODO completar
		return null;		
	}

	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		Queue<InfraccionesLocalizacion> ret = new Queue<InfraccionesLocalizacion>();
		MaxPQ<InfraccionesLocalizacion> pq = new MaxPQ<InfraccionesLocalizacion>();
		Iterator<VOMovingViolations> iter = mvQueue.iterator();

		while (iter.hasNext())
		{
			VOMovingViolations actual = iter.next();
			String x = actual.darXCoord();
			String y = actual.darYCoord();
			String loc = actual.getLocation();
			int address = actual.getAddressId();
			String stree = actual.getStreetSegId(); 

			Queue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
			Iterator<VOMovingViolations> iter2 = iter;
			while(iter2.hasNext() && iter2.next().darXCoord()==x && iter2.next().darYCoord()==y) 
			{
				cola.enqueue(iter2.next());
			}
			InfraccionesLocalizacion infrac= new InfraccionesLocalizacion(Double.parseDouble(x), Double.parseDouble(y), loc, address, Integer.parseInt(stree), cola);
			pq.insert(infrac);
		}
		N=N+1;
		while(N>0)
		{
			ret.enqueue(pq.delMax());
			N--;
		}
		return ret;
	}

	/**
	 * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	 * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 */
	public MaxPQ<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones()
	{
		// TODO completar
		// TODO Definir la Estructura Contenedora
		MaxPQ<InfraccionesViolationCode> retorno= new MaxPQ(new ComparadorViolationCode());
		IQueue<VOMovingViolations> copia= mvQueue;
		String[] codigos= new String[mvQueue.size()];
		for(int i=0;i<mvQueue.size();i++)
		{
			codigos[i]= copia.dequeue().getViolationCode();
		}
		//Sort.ordenarQuickSort(codigos);
		for (int i=0;i<codigos.length;i++)
		{
			if(codigos[i].equals(codigos[i++]))
			{
				
			}
		}
		
		return null;	
	}
}
